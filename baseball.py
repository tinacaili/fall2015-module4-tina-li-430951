import re
import sys

if len(sys.argv) != 2:
	print "Input file"
else:
	f = open(sys.argv[1])

	all_hits = dict()
	all_bats = dict()

	for line in f:
		name_re = re.compile(r"\b^[A-Z]\w*\s[A-Z]\w*\b")
		number_re = re.compile(r"\d")

		s = line.rstrip()

		number = number_re.findall(s)
		name = name_re.match(s)
		
		hits = 0
		bats = 0

		if number is not None:
			counter = 0
			for item in number:
				if (counter == 0):
					bats = int(item)
				if (counter == 1):
					hits = int(item)
				counter = counter + 1
		
		if name is not None:
			name = name.group(0)
			if name in all_hits:
				all_hits[name] = all_hits[name] + hits 
			else:
				all_hits[name] = hits
		
			if name in all_bats:
				all_bats[name] = all_bats[name] + bats
			else:
				all_bats[name] = bats

	ba = dict()
	for name in all_hits:
		avg = all_hits[name] / float(all_bats[name])
		ba[name] = avg

	sort = sorted(ba, key = ba.get, reverse = True)


	sorted_bat = dict()
	for name in sort:
		rounded = round(ba[name], 3)
		if len(str(rounded)) < 5:
			rounded = str(rounded) + "0"
		sorted_bat[name] = rounded
		print name, sorted_bat[name]
	f.close()
