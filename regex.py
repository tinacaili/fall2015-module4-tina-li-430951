import re

def find_literal(s):
	string_regex = re.compile(r"\bhello world\b")
	match = string_regex.findall(s)
	if match:
		print match
		return True
	else:
		print "No"
		return False

def find_triple(s):
	string_regex = re.compile(r"\b[^aeiou\W]*[aeiou]{3}[^aeiou\W]*\b")
	match = string_regex.findall(s)
	if match:
		print match
		return True
	else:
		print "No"
		return False

def find_airline(s):
	string_regex = re.compile(r"\bAA\d{3}\d?\b")
	match = string_regex.findall(s)
	if match:
		print match
		return True
	else:
		print "No"
		return False
		

find_literal("Programmers will often write hello world as their first project with a programming language.")
find_triple("The gooey peanut butter and jelly sandwich was a beauty.")
find_airline("AA312")
find_airline("AA1234")
find_airline("US1231")

